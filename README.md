Holland Homes Lake Martin builds new homes and provides remodel services to the Dadeville/Lake Martin area. Services include custom homes, kitchen and bath remodel, home additions, and more.

Address: Preserve at Stoney Ridge, Dadeville, AL 36853, USA

Phone: 256-853-0108

Website: http://www.hollandhomesllc.com/lakemartin
